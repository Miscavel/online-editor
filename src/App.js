import { useEffect, useState } from 'react';
import './App.css';
import CodeEditor from './component/codeEditor';
import GameWrapper from './component/gameWrapper';
import QuestionViewer from './component/questionViewer';
import ResultViewer from './component/resultViewer';
import { getQuestionData, submitQuestion } from './services/question';

function App() {
  const [ questionID, setQuestionID ] = useState(1);
  const [ questionData, setQuestionData ] = useState({
    description: 'Loading question data...',
    samples: {
      input: [],
      output: [],
    }
  });
  const [ questionResult, setQuestionResult ] = useState({
    result: undefined,
    error: undefined,
    successCount: 0,
    totalCount: 0,
    clear: false,
    isMock: false,
  });
  const [ questionAnswer, setQuestionAnswer ] = useState('');
  const [ loadingResult, setLoadingResult ] = useState(false);
  const [ loadingGame, setLoadingGame ] = useState(true);

  function reset() {
    setQuestionData({
      description: 'Loading question data...',
      samples: {
        input: [],
        output: [],
      }
    });
    setQuestionResult({
      result: undefined,
      error: undefined,
      successCount: 0,
      totalCount: 0,
      clear: false,
      isMock: false,
    });
    setQuestionAnswer('');
    setLoadingResult(false);
  }

  function submitAnswer(answer, isMock) {
    setQuestionAnswer(answer);
    setLoadingResult(true);
    submitQuestion(questionID, answer, isMock)
      .then((data) => {
        setQuestionResult(data);
      })
      .finally(() => {
        setLoadingResult(false);
      });
  }

  function loadQuestion(id) {
    reset();
    setLoadingResult(true);
    getQuestionData(id)
      .then((data) => {
        const { result } = data;
        setQuestionData(result);
        setQuestionAnswer(result.snippet);
      })
      .finally(() => {
        setLoadingResult(false);
      });
  }

  function loadRandomQuestion() {
    const newID = questionID >= 3 ? 1 : questionID + 1;
    setQuestionID(newID);
    loadQuestion(newID);
  }

  function onGameReady() {
    setLoadingGame(false);
  }

  useEffect(() => {
    loadQuestion(questionID);
  }, []);

  return (
    <div className='app'>
      <QuestionViewer 
        description={questionData.description}
        samples={questionData.samples}
      >
      </QuestionViewer>
      <div className='workspace'>
        <GameWrapper
          clear={questionResult.clear && !questionResult.isMock}
          onGameReady={onGameReady}
        >
        </GameWrapper>
        <CodeEditor
          snippet={questionAnswer}
          onSubmit={submitAnswer}
          onNextQuestion={loadRandomQuestion}
          loading={loadingResult || loadingGame}
          clear={questionResult.clear && !questionResult.isMock}
        >
        </CodeEditor>
        <ResultViewer
          loadingResult={loadingResult}
          result={questionResult.result}
          error={questionResult.error}
          successCount={questionResult.successCount}
          totalCount={questionResult.totalCount}
          clear={questionResult.clear}
          isMock={questionResult.isMock}
        >
        </ResultViewer>
      </div>
    </div>
  );
}

export default App;
