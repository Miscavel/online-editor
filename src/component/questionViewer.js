import SampleBox from './shared/sampleBox';

function QuestionViewer({ description, samples }) {
  return (
    <>
      <div className='question-viewer'>
        <div>
          <b>Task</b>
        </div>
        <div>
          { description }
        </div>
        <br></br>
        <div>
          <b>Sample Input</b>
        </div>
        <SampleBox
          content={samples.input}
        >
        </SampleBox>
        <br></br>
        <div>
          <b>Sample Output</b>
        </div>
        <SampleBox
          content={samples.output}
        >
        </SampleBox>
        <br></br>
        <div className='question-viewer-eol'>End of Line</div>
      </div>
    </>
  );
}

export default QuestionViewer;