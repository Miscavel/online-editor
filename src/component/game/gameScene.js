import * as Phaser from 'phaser';

export default class GameScene extends Phaser.Scene {
  score = 0;

  fireballs = 0;

  maxFireballs = 5;

  enemies = [];

  constructor() {
    super({
      key: 'GameScene',
    });
  }

  preload() {
    this.load.setBaseURL(`${window.location.href}/assets/game/`);
    this.load.spritesheet(
      'units', 
      'units.png', 
      {
        frameWidth: 64,
        frameHeight: 64,
      }
    );
    this.load.spritesheet(
      'tiles', 
      'tiles.png', 
      {
        frameWidth: 64,
        frameHeight: 64,
      }
    );
    this.load.image('bg', 'bg.png');
    this.load.image('cloud', 'cloud.png');
    this.load.image('mountain', 'mountain.png');
    this.load.image('ui_bar', 'ui_bar.png');
  }

  create() {
    this.createBG();
    this.createMountains();
    this.createClouds();
    this.createPlayer();
    this.createEnemies();
    this.createFireballs();
    this.createTiles();
    this.createScoreBar();

    this.game.events.emit('game_ready');
  }

  createBG() {
    this.add.image(640, 0, 'bg').setScale(2);
  }

  createMountains() {
    const mountainData = [
      {
        x: 320,
        y: -60,
        velocityX: -13
      },
      {
        x: 960,
        y: -60,
        velocityX: -13
      },
      {
        x: 1600,
        y: -60,
        velocityX: -13
      },
      {
        x: 2240,
        y: -60,
        velocityX: -13
      },
    ];

    const mountains = [];
    mountainData.forEach((data) => {
      const { x, y, velocityX } = data;
      const mountain = this.physics.add.image(x, y, 'mountain');
      mountain.setVelocityX(velocityX);
      mountains.push(mountain);
    });

    this.events.on(Phaser.Scenes.Events.POST_UPDATE, () => {
      mountains.forEach((mountain) => {
        if (mountain.x < -320) {
          const maxX = mountains.reduce((res, ele) => {
            return Math.max(res, ele.x);
          }, Number.MIN_SAFE_INTEGER);
          mountain.setPosition(maxX + 640, mountain.y);
        }
      });
    });
  }

  createClouds() {
    const { displayWidth } = this.cameras.main;

    const cloudData = [
      {
        x: 212,
        y: 36,
        velocityX: -10
      },
      {
        x: 1100,
        y: 18,
        velocityX: -15
      },
      {
        x: 1800,
        y: 160,
        velocityX: -20
      },
    ];

    const clouds = [];
    cloudData.forEach((data) => {
      const { x, y, velocityX } = data;
      const cloud = this.physics.add.image(x, y, 'cloud');
      cloud.setVelocityX(velocityX);
      clouds.push(cloud);
    });

    this.events.on(Phaser.Scenes.Events.POST_UPDATE, () => {
      clouds.forEach((cloud) => {
        if (cloud.x < -300) {
          cloud.setPosition(displayWidth + 300, cloud.y);
        }
      });
    });
  }

  createPlayer() {
    const { displayHeight } = this.cameras.main;
    this.anims.create({
      key: 'player_run',
      frames: this.anims.generateFrameNumbers('units', {
        start: 20,
        end: 22,
      }),
      frameRate: 8,
      repeat: -1,
    });
    this.anims.create({
      key: 'player_hurt',
      frames: this.anims.generateFrameNumbers('units', {
        start: 40,
        end: 41,
      }),
      frameRate: 8,
    });

    const player = this.add.sprite(48, displayHeight - 96, 'units', 0);
    player.anims.play('player_run');
    player.on(Phaser.Animations.Events.ANIMATION_COMPLETE, (anim) => {
      if (anim.key === 'player_hurt') {
        this.time.delayedCall(300, () => {
          player.anims.play('player_run');
        });
      }
    });

    this.events.on('player_hurt', () => {
      player.anims.play('player_hurt');
    });
  }

  createEnemies() {
    const { displayHeight } = this.cameras.main;
    this.anims.create({
      key: 'enemy_run',
      frames: this.anims.generateFrameNumbers('units', {
        start: 3,
        end: 5,
      }),
      frameRate: 8,
      repeat: -1,
    });

    const enemyData = [
      {
        x: 1600,
        y: displayHeight - 96,
        velocityX: -100
      },
      {
        x: 2240,
        y: displayHeight - 96,
        velocityX: -100
      },
      {
        x: 2880,
        y: displayHeight - 96,
        velocityX: -100
      },
      {
        x: 3520,
        y: displayHeight - 96,
        velocityX: -100
      },
      {
        x: 4160,
        y: displayHeight - 96,
        velocityX: -100
      },
    ];

    this.enemies = [];
    const maxHP = 3;
    enemyData.forEach((data) => {
      const { x, y, velocityX } = data;
      const enemy = this.physics.add.sprite(x, y, 'units', 3);
      enemy.setVelocityX(velocityX);
      enemy.setData('hp', maxHP);
      enemy.on('hurt', (damage) => {
        const remainingHP = enemy.getData('hp') - damage;
        if (remainingHP <= 0) {
          enemy.setData('hp', maxHP);
          const maxX = this.enemies.reduce((res, ele) => {
            return Math.max(res, ele.x);
          }, Number.MIN_SAFE_INTEGER);
          enemy.setPosition(maxX + 640, enemy.y);
          this.events.emit('enemy_die');
        } else {
          enemy.setData('hp', remainingHP);
        }
      });
      enemy.anims.play('enemy_run');
      this.enemies.push(enemy);
    });

    this.events.on(Phaser.Scenes.Events.POST_UPDATE, () => {
      this.enemies.forEach((enemy) => {
        if (enemy.x < 100) {
          enemy.setData('hp', maxHP);
          const maxX = this.enemies.reduce((res, ele) => {
            return Math.max(res, ele.x);
          }, Number.MIN_SAFE_INTEGER);
          enemy.setPosition(maxX + 640, enemy.y);
          this.events.emit('player_hurt');
        }
      });
    });
  }

  createFireballs() {
    const { displayHeight } = this.cameras.main;
    this.anims.create({
      key: 'fireball',
      frames: this.anims.generateFrameNumbers('units', {
        start: 80,
        end: 82,
      }),
      frameRate: 8,
      yoyo: true,
      repeat: -1,
    });
    
    this.time.addEvent({
      delay: 3000,
      callback: () => {
        for (let i = 0; i < this.fireballs; i++) {
          const fireball = this.physics.add.sprite(60, displayHeight - 96, 'units', 80);
          fireball.anims.play('fireball');
          fireball.setVisible(false);
          this.time.delayedCall(600 * i, () => {
            fireball.setVisible(true);
            fireball.setVelocityX(100);
          });

          const onPostUpdate = () => {
            if (fireball.x >= 1000) {
              fireball.destroy();
              this.events.removeListener(Phaser.Scenes.Events.POST_UPDATE, onPostUpdate);
            }
            this.enemies.forEach((enemy) => {
              if (enemy.x - fireball.x <= 32) {
                enemy.emit('hurt', 1);
                fireball.destroy();
                this.events.removeListener(Phaser.Scenes.Events.POST_UPDATE, onPostUpdate);
              }
            });
          };

          this.events.on(Phaser.Scenes.Events.POST_UPDATE, onPostUpdate);
        }
      },
      loop: true
    });

    const instructionText = this.add.text(
      10, 
      0, 
      'Clear questions to get weapon', 
      { 
        fontFamily: "Visitor",
        fontSize: '36px',
        color: '#534741'
      }
    );

    this.game.events.on('clear_question', () => {
      if (this.fireballs === 0) {
        instructionText?.destroy();
      }
      if (this.fireballs < this.maxFireballs) {
        this.add.image(32 + 64 * this.fireballs, 32, 'units', 70);
        this.fireballs += 1;
      }
    });
  }

  createTiles() {
    const { displayWidth, displayHeight } = this.cameras.main;
    for (let i = 0; i <= displayWidth; i += 64) {
      this.add.image(i, displayHeight - 32, 'tiles', 1);
    }
  }

  createScoreBar() {
    const { displayWidth } = this.cameras.main;
    const uiBar = this.add.image(displayWidth, 0, 'ui_bar').setScale(0.5).setOrigin(1, 0);
    const text = this.add.text(
      displayWidth - 250, 
      10, 
      'SCORE: 0', 
      { 
        fontFamily: "Visitor",
        fontSize: '36px',
        color: '#534741'
      }
    ).setOrigin(0, 0);
    
    this.events.on('player_hurt', () => {
      this.setScore(this.score - 10);
      text.setText('SCORE: ' + this.score);
    });

    this.events.on('enemy_die', () => {
      this.setScore(this.score + 10);
      text.setText('SCORE: ' + this.score);
    });
  }

  setScore(score) {
    this.score = Math.max(score, 0);
  }
}
