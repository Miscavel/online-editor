import * as Phaser from 'phaser';
import { useEffect, useState } from 'react';
import GameScene from './game/gameScene';

function GameWrapper({ clear, onGameReady }) {
  const [ game, setGame ] = useState(undefined);
  useEffect(() => {
    if (!game) {
      const newGame = new Phaser.Game({
        title: 'Idle Game',
        scale: {
          parent: 'game',
          mode: Phaser.Scale.FIT,
          autoCenter: Phaser.Scale.CENTER_BOTH,
          width: 1280,
          height: 256,
        },
        type: Phaser.AUTO,
        physics: {
          default: 'arcade',
          arcade: {
            debug: false,
          },
        },
        scene: [GameScene],
        backgroundColor: '#0B1728',
      });
      newGame.events.once('game_ready', () => {
        onGameReady();
      });
      setGame(newGame);
    }
  }, []);

  if (clear) {
    game?.events.emit('clear_question');
  }

  return (
    <div id='game' className='game-wrapper'>
    </div>
  );
}

export default GameWrapper;