function LinkWrapper({ links }) {
  return (
    <div className='link-wrapper'>
      {
        links &&
        links.map((link, index) => {
          const { url, href } = link;
          return (
            <div className='link-content' key={index}>
              <a href={href} target='_blank'>
                <img src={url} className='link-img'></img>
              </a>
            </div>
          );
        })
      }
    </div>
  );
}

export default LinkWrapper;