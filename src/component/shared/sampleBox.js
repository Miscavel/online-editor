function SampleBox({ content }) {
  return (
    <div className='sample-box'>
      {
        content &&
        content.map((entry, index) => {
          return (
            <div key={index}>
              Case #{index}: { entry }
            </div>
          )
        })
      }
    </div>
  )
}

export default SampleBox;