import CrossIcon from '../assets/cross.png';
import CheckIcon from '../assets/check.png';
import SpinnerIcon from '../assets/spinner.gif';

function ResultViewer({ 
  loadingResult,
  result, 
  error,
  successCount,
  totalCount,
  clear,
  isMock
}) {
  // Scroll to top on loading
  if (loadingResult) {
    document.querySelector('.result-viewer').scrollTop = 0;
  }

  return (
    <>
      <div className='result-viewer'>
        {
          loadingResult &&
          <>
            <div className='result-viewer-loading'>
            </div>
            <div className='result-viewer-loading-spinner'>
              <img className='result-viewer-loading-spinner-icon' src={SpinnerIcon}></img>
            </div>
          </>
        }
        {
          result &&
          <>
            <div className={`result-viewer-header ${clear ? 'clear' : ''}`}>
              { isMock ? 'Test ' : '' }Result: {successCount} / {totalCount}
            </div>
            <div className='result-viewer-content-outer'>
              <div className='result-viewer-content'>
                { 
                  result.map((entry, index) => {
                    const { params, compiledOutput, expectedOutput, success, error } = entry;
                    const output = String(compiledOutput ?? error);
                    const paramString = params ? params.join(' ') : 'Hidden';
                    const testCaseLabel = isMock? 'Sample Test Case' : 'Test Case';
                    return (
                      <div className='result-viewer-entry' key={index}>
                        <div className='result-viewer-entry-content'>
                          <div>
                            {testCaseLabel} #{index}: 
                          </div>
                          <div>Input: {paramString}</div>
                          <div>Output: {output}</div>
                          <div>Expected Output: {String(expectedOutput)}</div>
                        </div>
                        <div className='result-viewer-entry-info'>
                          <img 
                              src={ success ? CheckIcon : CrossIcon } 
                              className='result-viewer-entry-icon'
                          >
                          </img>
                        </div>
                      </div>
                    );
                  })
                }
              </div>
            </div>
          </>
        }
        {
          error &&
          <div>{ error }</div>
        }
      </div>
    </>
  );
}

export default ResultViewer;