import { basicSetup } from "codemirror"
import { EditorView, keymap } from "@codemirror/view";
import { indentWithTab } from "@codemirror/commands";
import { EditorState } from "@codemirror/state";
import { useState, useEffect } from "react";
import { javascript } from "@codemirror/lang-javascript"

function CodeEditor({ snippet, onSubmit, onNextQuestion, loading, clear }) {
  const [ codeEditor, setCodeEditor ] = useState(undefined);

  function submitAnswer() {
    onSubmit(getEditorContent(), false);
  }

  function submitTests() {
    onSubmit(getEditorContent(), true);
  }

  function getEditorContent() {
    if (!codeEditor) return '';

    return codeEditor.state.doc.toString();
  }

  function updateEditorWithSnippet() {
    if (codeEditor) {
      const contentLength = getEditorContent().length;
      codeEditor.dispatch({
        changes: {
          from: 0, 
          to: contentLength,
          insert: `${snippet}`
        }
      });
    }
  }

  useEffect(() => {
    if (!codeEditor) {
      const editor = new EditorView({
        state: EditorState.create({
          doc: `${snippet}`,
          extensions: [
            basicSetup,
            keymap.of([indentWithTab]),
            javascript(),
          ]
        }),
        parent: document.querySelector('#codemirror-editor')
      });
      setCodeEditor(editor);
    }
  }, []);

  updateEditorWithSnippet();

  return (
    <>
      <div className='code-editor'>
        <div id='codemirror-editor' className='codemirror-editor'></div>
        <div className='code-editor-button-group'>
          <button
            onClick={submitTests}
            disabled={loading || clear}
            className='button run'
          >
            Run Code
          </button>
          <button
            onClick={submitAnswer}
            disabled={loading || clear}
            className='button submit'
          >
            Submit Code
          </button>
          <button
            onClick={onNextQuestion}
            disabled={loading || !clear}
            className='button next'
          >
            Next Question
          </button>
        </div>
      </div>
    </>
  )
}

export default CodeEditor;