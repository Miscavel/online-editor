const baseUrl = 'https://nodejs-js-compiler.herokuapp.com';

export async function getQuestionData(id) {
  const url = `${baseUrl}/question/${id}`;

  const response = await fetch(url);

  if (!response.ok) throw new Error('Fail to fetch question data');

  return await response.json();
}

export async function submitQuestion(id, answer, isMock) {
  const url = `${baseUrl}/submit/${id}`;

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ answer, isMock }),
    headers: {
      'Content-Type': 'application/json'
    }
  });

  if (!response.ok) throw new Error('Fail to submit question');

  return await response.json();
}